FROM openjdk:12-alpine
COPY mvnjar/assignment-*.jar /assignment.jar
# COPY target/assignment-*.jar /assignment.jar
ENV HOST=
ENV USER=
ENV PASSWORD=
CMD java -jar -Dspring.datasource.url=jdbc:mysql://${HOST}/assignment?allowPublicKeyRetrieval=true -Dspring.datasource.username=${USER} -Dspring.datasource.password=${PASSWORD} assignment.jar